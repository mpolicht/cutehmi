#ifndef RECENCYMODEL_HPP
#define RECENCYMODEL_HPP

#include "internal/common.hpp"

#include "AbstractDataModel.hpp"

namespace cutehmi {
namespace dataacquisition {

class CUTEHMI_DATAACQUISITION_API RecencyModel:
	public AbstractDataModel
{
		Q_OBJECT

		typedef AbstractDataModel Parent;

	public:
		enum Role {
			TAG_ROLE = Qt::UserRole,
			VALUE_ROLE,
			TIME_ROLE
		};

		Q_PROPERTY(int min READ min WRITE setMin NOTIFY minChanged)

		Q_PROPERTY(int max READ max WRITE setMax NOTIFY maxChanged)

		Q_PROPERTY(int from READ from WRITE setFrom NOTIFY fromChanged)

		Q_PROPERTY(int to READ to WRITE setTo NOTIFY toChanged)

		RecencyModel(QObject * parent = nullptr);

		int min() const;

		void setMin(int min);

		int max() const;

		void setMax(int max);

		int from() const;

		void setFrom(int from);

		int to() const;

		void setTo(int to);

		QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;

		QHash<int, QByteArray> roleNames() const override;

	signals:
		void minChanged();

		void maxChanged();

		void fromChanged();

		void toChanged();

	public slots:
		void update() override;

	private:
		struct Members {
			int min;
			int max;
			int from;
			int to;
		};

		MPtr<Members> m;
};

}
}

#endif // RECENCYMODEL_HPP
