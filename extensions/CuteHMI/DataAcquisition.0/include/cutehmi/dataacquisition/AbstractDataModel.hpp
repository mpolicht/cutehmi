#ifndef DATAMODEL_HPP
#define DATAMODEL_HPP

#include "internal/common.hpp"

#include <QAbstractListModel>

namespace cutehmi {
namespace dataacquisition {

class CUTEHMI_DATAACQUISITION_API AbstractDataModel:
	public QAbstractListModel
{
		Q_OBJECT

	public:
		AbstractDataModel(QObject * parent = nullptr);

		int rowCount(const QModelIndex & parent = QModelIndex()) const override;

	public slots:
		virtual void update() = 0;

	protected:
		void setRowCount(int rowCount);

	private:
		struct Members {
			int rowCount;
		};

		MPtr<Members> m;
};

}
}

#endif // DATAMODEL_HPP
