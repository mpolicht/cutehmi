#include <cutehmi/dataacquisition/RecencyModel.hpp>

namespace cutehmi {
namespace dataacquisition {

RecencyModel::RecencyModel(QObject * parent):
	AbstractDataModel(parent),
	m(new Members{0, 0, 0, 0})
{
}

int RecencyModel::min() const
{
	return m->min;
}

void RecencyModel::setMin(int min)
{
	if (m->min != min) {
		m->min = min;
		emit minChanged();
	}
}

int RecencyModel::max() const
{
	return m->max;
}

void RecencyModel::setMax(int max)
{
	if (m->max != max) {
		m->max = max;
		emit maxChanged();
	}
}

int RecencyModel::from() const
{
	return m->from;
}

void RecencyModel::setFrom(int from)
{
	if (m->from != from) {
		m->from = from;
		emit fromChanged();
	}
}

int RecencyModel::to() const
{
	return m->to;
}

void RecencyModel::setTo(int to)
{
	if (m->to != to) {
		m->to = to;
		emit toChanged();
	}
}

QVariant RecencyModel::data(const QModelIndex & index, int role) const
{
	if (!index.isValid())
		return QVariant();

	return QVariant();
}

QHash<int, QByteArray> RecencyModel::roleNames() const
{
	QHash<int, QByteArray> result = Parent::roleNames();
	result[TAG_ROLE] = "tag";
	result[VALUE_ROLE] = "value";
	result[TIME_ROLE] = "time";
	return result;
}

void RecencyModel::update()
{

}

}
}
