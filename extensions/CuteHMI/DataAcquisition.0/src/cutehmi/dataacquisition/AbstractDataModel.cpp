#include <cutehmi/dataacquisition/AbstractDataModel.hpp>

namespace cutehmi {
namespace dataacquisition {

AbstractDataModel::AbstractDataModel(QObject * parent):
	QAbstractListModel(parent),
	m(new Members{0})
{
}

int AbstractDataModel::rowCount(const QModelIndex & parent) const
{
	Q_UNUSED(parent)

	return m->rowCount;
}

void AbstractDataModel::setRowCount(int rowCount)
{
	m->rowCount = rowCount;
}

}
}
