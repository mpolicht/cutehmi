#ifndef INTERNATIONALIZATOR_HPP
#define INTERNATIONALIZATOR_HPP

#include "internal/common.hpp"
#include "Singleton.hpp"

#include <QObject>

namespace cutehmi {

class CUTEHMI_API Internationalizator:
	public QObject,
	public Singleton<Internationalizator>
{
		Q_OBJECT

		friend class Singleton<Internationalizator>;

	protected:
		Internationalizator() = default;
};

}

#endif // INTERNATIONALIZATION_HPP
